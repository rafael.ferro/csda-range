# -*- coding: utf-8 -*-
"""
Created on Sat May 19 15:06:18 2018

@author: rafael
"""

from io import StringIO
import requests
import numpy as np
from scipy.integrate import quad
from scipy.interpolate import interp1d

NAVOGADRO = 6.022141e23  # mol^-1


# %%
def pstar(Z, Tpi):
    '''Retrieve data for CSDA Range (g/cm2) for protons from NIST database [1].

    Z = atomic number of the target element.
    Tpi = initial kinetic energy (MeV) of the impinging proton

    [1] https://physics.nist.gov/PhysRefData/Star/Text/PSTAR.html
    '''

    energies_str = ''
    energies_dict = {}
    energies_array = np.linspace(1e-3, Tpi, 500)
    for i, en in enumerate(energies_array):
        energies_str = energies_str + '%s' % en + '%0A'
        energies_dict['E%s' % i] = '%s' % en

    params1 = {
        'matno': '%s' % Z, 'Energies': '%s' % energies_str,
        'GraphType': 'None', 'ShowDefault': ''
    }
    params2 = {
        'matno': '%s' % Z, 'NumofEnergies': '%s' % (len(energies_array)),
        'ShowDefault': '', 'csda': 'on',
        'character': 'tab', 'prog': 'PSTAR'
    }
    params2 = dict(params2, **energies_dict)

    with requests.Session() as s:
        s.headers = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64;\
                     rv:60.0) Gecko/20100101 Firefox/60.0'}

        r = s.get("https://physics.nist.gov/PhysRefData/Star/Text/PSTAR.html")

        r = s.post("https://physics.nist.gov/cgi-bin/Star/ap_table.pl",
                   data=params1)

        r = s.post("https://physics.nist.gov/cgi-bin/Star/apdata.pl",
                   data=params2)

        table = StringIO(r.text)

    energy = np.array([])
    csda = np.array([])
    for line in table.readlines()[8:]:
        energy = np.append(energy, float(line.split('\t')[0]))
        csda = np.append(csda, float(line.split('\t')[1]))
    csda = csda * 1e6  # convert g/cm2 to ug/cm2
    return(energy, csda)


# %%
def srim(Z, Tpi):
    '''Retrieve data of stopping power for protons from SRIM database [2].

    Z = atomic number of the target element.
    Tpi = initial kinetic energy (MeV) of the impinging proton

    [2] http://www.srim.org/
    '''
    en, sn, se = np.genfromtxt(
        "./SRIM/{}_p_srim.dat".format(Z),
        skip_header=4,
        unpack=True,
        dtype=None
    )

    st = (sn + se)  # 1e-15 eV/(atoms/cm2)
    f = 1/st * 1e3  # convert 1/eV to 1/keV
    func = interp1d(en, f)
    a = min(en)
    b = Tpi * 1e3  # convert MeV to keV
    csda, error = quad(func, a, b, epsabs=1e-6, epsrel=1e-6, limit=int(1e6))

    ELEMDATA = np.genfromtxt(
        'Z_Symb_Element_AtomicMass_MeanI_Density.csv',
        skip_header=2,
        dtype=(int, 'U8', 'U8', float, float),
        delimiter='\t'
    )

    ATOMICMASS = [M[3] for M in ELEMDATA if M[0] == Z][0]
    # convert 1e15 atoms/cm2 to ug/cm2
    return (csda*1e15*ATOMICMASS/NAVOGADRO*1e6)


# %%
def csda(Z, Tpi, source='auto'):
    '''Try to retrieve data from NIST database [1]. If unavailable,
    calculate CSDA Range (ug/cm2) for protons from SRIM stopping
    power database [2].

    params:

    Z: atomic number of the target element.
    Tpi: initial kinetic energy (MeV) of the impinging proton
    source: 'auto', 'pstar', 'srim'

    [1] https://physics.nist.gov/PhysRefData/Star/Text/PSTAR.html
    [2] http://www.srim.org/
    '''
    if source == 'pstar':
        energy, csdarange = pstar(Z, Tpi)
        if not csdarange.any():
            print('Unavailable data for Z = %s in NIST database.' % Z)
            return 1
    elif source == 'srim':
        csdarange = srim(Z, Tpi)
        energy = Tpi
    else:
        energy, csdarange = pstar(Z, Tpi)
        if not csdarange.any():
            print(
                'Unavailable data for Z = %s in NIST database.\n'
                'Using SRIM database to calculate the CSDA Range.' % Z
            )
            csdarange = srim(Z, Tpi)
            energy = Tpi
    return(energy, csdarange)


# %%
if __name__ == '__main__':
    csdarange = csda(29, 4, source='srim')
    print(csdarange)
    csdarange = csda(28, 4, source='srim')
    print(csdarange)
    csdarange = csda(28, 4, source='auto')
    print(csdarange)
    csdarange = csda(28, 4, source='pstar')
    print(csdarange)
