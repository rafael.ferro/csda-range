# Proton energy loss from CSDA Range

Calculate the energy loss of a proton, given its initial energy and the thickness of the target.
The CSDA Range for protons are calculated based on the stopping power values from the [SRIM](http://www.srim.org/) database.

## References:

1. [SRIM](http://www.srim.org/)
2. [NIST](https://physics.nist.gov/PhysRefData/Star/Text/PSTAR.html)

